package prof.code.enums;


public enum DictionaryType {
    LATIN_DICTIONARY("LatinDictionary"),
    DIGITAL_DICTIONARY("DigitalDictionary"),
    FREE_DICTIONARY("FreeDictionary");

    private String value;

    DictionaryType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
