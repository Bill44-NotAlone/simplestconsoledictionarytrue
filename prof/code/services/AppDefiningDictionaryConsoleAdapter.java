package prof.code.services;

import prof.code.classes.AppDefiningDictionaryAbstractAdapter;
import prof.code.classes.AppDefiningDictionarySettings;
import prof.code.enums.DictionaryType;
import prof.code.interfaces.IDefiningDictionaryInformant;
import prof.code.model.Word;
import prof.code.classes.intput.console.ConsoleInputDefiningDictionary;
import prof.code.classes.intput.console.ConsoleDefiningDictionaryInformant;

import java.util.Scanner;
import java.util.regex.Pattern;

public class AppDefiningDictionaryConsoleAdapter extends AppDefiningDictionaryAbstractAdapter {
    private final Scanner scanner;

    public AppDefiningDictionaryConsoleAdapter(DefiningDictionaryService definingDictionaryService) {
        settings = new AppDefiningDictionarySettings();
        settings.setDictionary(definingDictionaryService);
        settings.setInformant(new ConsoleDefiningDictionaryInformant());
        settings.setWordInput(new ConsoleInputDefiningDictionary());
        scanner = new Scanner(System.in);
    }

    @Override
    public void run() {
        IDefiningDictionaryInformant informant = settings.getInformant();
        System.out.print(informant.getInfoDictionary());
        settings.setDictionaryType(DictionaryType.values()[Integer.parseInt(scanner.nextLine()) - 1]);
        System.out.print(informant.getInfo());
        while (true) {
            String input = scanner.nextLine();
            if (checkReservedWords(input))
                continue;
            Word word = settings.getWordInput().shaping(input);
            if (word != null && settings.getDictionary().saveData(word) != null)
                continue;
            System.out.println("Строка. Это строка... Это что вообще?");
        }
    }

    public boolean checkReservedWords(String input) {
        DefiningDictionaryService dictionary = settings.getDictionary();
        if (input.contentEquals( "exit")){
            System.exit(0);
        }
        if (input.contentEquals("get all")){
            dictionary.getAll().forEach(w -> System.out.printf("%s - %s\n", w.getValue(), w.getDefinition()));
            return true;
        }
        if (Pattern.matches("^remove .+$", input)){
            dictionary.remove(input.split(" ")[1]);
            return true;
        }
        if (Pattern.matches("^find .+$", input)){
            Word word = dictionary.find(input.split(" ")[1]);
            if (word == null)
                System.out.println("Нет такого слова.");
            else
                System.out.printf("%s - %s\n", word.getValue(), word.getDefinition());
            return true;
        }
        if (Pattern.matches("^change \\d$", input)){
            String[] keyCommand =  input.split(" ");
            if (Pattern.matches("^[1-3]", keyCommand[1]))
                settings.setDictionaryType(DictionaryType.values()[Integer.parseInt(keyCommand[1]) - 1]);
            else
                System.out.println("Словоря не существует.");
            return true;
        }
        return false;
    }
}
