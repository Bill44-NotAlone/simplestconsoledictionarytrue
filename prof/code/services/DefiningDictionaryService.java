package prof.code.services;

import prof.code.interfaces.IDefiningDictionaryAbstractActionService;
import prof.code.interfaces.IDefiningDictionaryWordValidator;
import prof.code.interfaces.IWordRepository;
import prof.code.model.Word;

import java.util.List;

public class DefiningDictionaryService implements IDefiningDictionaryAbstractActionService {
    private IDefiningDictionaryWordValidator wordValidator;
    private IWordRepository dataSourceServiceDictionary;

    public void setWordValidator(IDefiningDictionaryWordValidator wordValidator) {
        this.wordValidator = wordValidator;
    }

    public void setDataSourceServiceDictionary(IWordRepository dataSourceServiceDictionary) {
        this.dataSourceServiceDictionary = dataSourceServiceDictionary;
    }

    public DefiningDictionaryService() {

    }

    public DefiningDictionaryService(IWordRepository dataSourceServiceDictionary, IDefiningDictionaryWordValidator wordValidator) {
        this.wordValidator = wordValidator;
        this.dataSourceServiceDictionary = dataSourceServiceDictionary;
    }

    public Word saveData(Word word) {
        return wordValidator.check(word.getValue()) ? dataSourceServiceDictionary.saveData(word) : null;
    }

    public Word remove(String value) {
        return dataSourceServiceDictionary.remove(new Word(value, null));
    }

    public List<Word> getAll() {
        return dataSourceServiceDictionary.getAll();
    }

    public Word find(String value) {
        return dataSourceServiceDictionary.find(value);
    }
}
