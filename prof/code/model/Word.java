package prof.code.model;

public class Word {
    private String value;
    private String definition;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Word() { }

    public Word(String value, String definition) {
        this.value = value;
        this.definition = definition;
    }
}
