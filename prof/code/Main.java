package prof.code;

import prof.code.services.AppDefiningDictionaryConsoleAdapter;
import prof.code.interfaces.IAdapter;
import prof.code.services.DefiningDictionaryService;

public class Main {
    public static void main(String[] args) {
        IAdapter iAdapter = new AppDefiningDictionaryConsoleAdapter(new DefiningDictionaryService());
        iAdapter.run();
    }
}
