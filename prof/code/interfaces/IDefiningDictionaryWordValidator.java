package prof.code.interfaces;

public interface IDefiningDictionaryWordValidator {
    public boolean check(String value);
}
