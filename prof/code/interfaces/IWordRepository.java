package prof.code.interfaces;

import prof.code.model.Word;
import java.util.List;

public interface IWordRepository {
    public Word saveData(Word word);
    public Word remove(Word word);
    public List<Word> getAll();
    public Word find(String value);
}
