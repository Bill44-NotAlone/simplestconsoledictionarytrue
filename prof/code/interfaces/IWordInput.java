package prof.code.interfaces;

import prof.code.model.Word;

public interface IWordInput {
    public Word shaping(Object input);
}
