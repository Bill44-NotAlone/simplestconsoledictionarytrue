package prof.code.interfaces;

import prof.code.model.Word;

import java.util.List;

public interface IDefiningDictionaryAbstractActionService {
    public Word saveData(Word word);
    public Word remove(String value);
    public List<Word> getAll();
    public Word find(String value);
}
