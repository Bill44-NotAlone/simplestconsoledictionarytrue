package prof.code.interfaces;

public interface IDefiningDictionaryInformant {
    public String getInfo();
    public String getInfoDictionary();
}
