package prof.code.interfaces;

public interface IAdapter {
    public void run();
}