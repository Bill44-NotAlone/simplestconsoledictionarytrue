package prof.code.classes;

import prof.code.classes.validation.BinaryDefiningDictionaryWordValidator;
import prof.code.classes.validation.CommonDefiningDictionaryWordValidator;
import prof.code.classes.validation.LatinDefiningDictionaryWordValidator;
import prof.code.classes.wordaccess.TxtFileWordRepository;
import prof.code.enums.DictionaryType;
import prof.code.interfaces.IDefiningDictionaryInformant;
import prof.code.interfaces.IWordInput;
import prof.code.services.DefiningDictionaryService;

public class AppDefiningDictionarySettings {
    protected DefiningDictionaryService dictionary;
    protected DictionaryType dictionaryType;
    protected IDefiningDictionaryInformant informant;
    protected IWordInput wordInput;

    public DefiningDictionaryService getDictionary() {
        return dictionary;
    }

    public void setDictionary(DefiningDictionaryService dictionary) {
        this.dictionary = dictionary;
    }

    public DictionaryType getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(DictionaryType dictionaryType) {
        this.dictionaryType = dictionaryType;
        changeDictionaryType(dictionaryType);
    }

    public IDefiningDictionaryInformant getInformant() {
        return informant;
    }

    public void setInformant(IDefiningDictionaryInformant informant) {
        this.informant = informant;
    }

    public IWordInput getWordInput() {
        return wordInput;
    }

    public void setWordInput(IWordInput wordInput) {
        this.wordInput = wordInput;
    }

    private void changeDictionaryType(DictionaryType dictionaryType){
        if (dictionary != null) {
            switch (dictionaryType) {
                case DIGITAL_DICTIONARY:
                    dictionary.setWordValidator(new BinaryDefiningDictionaryWordValidator());
                    break;
                case FREE_DICTIONARY:
                    dictionary.setWordValidator(new CommonDefiningDictionaryWordValidator());
                    break;
                case LATIN_DICTIONARY:
                    dictionary.setWordValidator(new LatinDefiningDictionaryWordValidator());
                    break;
            }
            dictionary.setDataSourceServiceDictionary(new TxtFileWordRepository(dictionaryType.getValue()));
        }
    }
}
