package prof.code.classes.wordaccess;

import prof.code.interfaces.IWordRepository;
import prof.code.model.Word;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TxtFileWordRepository implements IWordRepository {
    private String fileName;

    public TxtFileWordRepository(String fileName) {
        setFileName(fileName);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName + ".txt";
    }

    @Override
    public Word saveData(Word word) {
        try {
            List<Word> words = getAll();
            Word duplicate = words.stream().filter(w -> w.getValue().contentEquals(word.getValue())).findFirst().orElse(null);
            if (duplicate != null)
                duplicate.setDefinition(word.getDefinition());
            else
                words.add(word);
            rewrite(words);
        } catch (FileNotFoundException e) {
            try {
                Files.createFile(Path.of(fileName));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return word;
    }

    @Override
    public Word remove(Word word) {
        try {
            List<Word> words = getAll();
            words.removeIf(w -> w.getValue().contentEquals(word.getValue()));
            rewrite(words);
        } catch (FileNotFoundException e) {
            try {
                Files.createFile(Path.of(fileName));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return word;
    }

    @Override
    public List<Word> getAll() {
        List<Word> words = new ArrayList<>();
        if (Files.exists(Paths.get(fileName))) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new FileReader(fileName));
            } catch (FileNotFoundException e) {
                try {
                    Files.createFile(Path.of(fileName));
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
            bufferedReader.lines().forEach(l ->{
                String[] strings = l.split(" - ", 2);
                words.add(new Word(strings[0], strings[1]));
            });
        }
        return words;
    }

    @Override
    public Word find(String value) {
        return getAll().stream().filter(w -> w.getValue().contentEquals(value)).findFirst().orElse(null);
    }

    private void rewrite(List<Word> words) throws FileNotFoundException {
        PrintWriter file = new PrintWriter(fileName);
        words.stream().forEach(word -> file.write(String.format("%s - %s\n", word.getValue(), word.getDefinition())));
        file.close();
    }
}
