package prof.code.classes.intput.console;

import prof.code.interfaces.IWordInput;
import prof.code.model.Word;

import java.util.regex.Pattern;

public class ConsoleInputDefiningDictionary implements IWordInput {
    @Override
    public Word shaping(Object input) {
        String inputString = input.toString();
        if (Pattern.matches("^.*-( )?.[^-]+$", inputString)){
            String[] pair = inputString.split("( )?-( )?");
            return new Word(pair[0].trim(), pair[1].trim());
        }
        if (Pattern.matches("^.[^ -]* .[^-]*$", inputString)) {
            String[] pair = inputString.split(" ", 2);
            return new Word(pair[0].trim(), pair[1].trim());
        }
        return null;
    }
}
