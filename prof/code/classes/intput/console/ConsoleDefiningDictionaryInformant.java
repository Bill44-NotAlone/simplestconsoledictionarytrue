package prof.code.classes.intput.console;

import prof.code.interfaces.IDefiningDictionaryInformant;

public class ConsoleDefiningDictionaryInformant implements IDefiningDictionaryInformant {
    @Override
    public String getInfo() {
        return "Формат данных:\n Слово значение \n Слово - значение \n Просмотр предыдущих записей : введите get all \n Что бы удалить слово нужно ввеси remove само_слово \n Что бы найти слово введите find само_слово \n Что бы сменить формат ввода наберите change индекс_словоря \n Выйти из приложения: введите exit \n";
    }

    @Override
    public String getInfoDictionary() {
        return "Выберете в каком режиме создавать словарь? \n 1 - длинна слов может быть только 4 символа и эти символы только буквы латинской раскладки \n 2 - длина слов может быть только 5 символа и эти символы только цифры \n 3 - свободный \n";
    }
}
