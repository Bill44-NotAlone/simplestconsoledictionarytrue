package prof.code.classes.validation;

import prof.code.interfaces.IDefiningDictionaryWordValidator;

import java.util.regex.Pattern;

public class BinaryDefiningDictionaryWordValidator implements IDefiningDictionaryWordValidator {
    @Override
    public boolean check(String value) {
        return Pattern.matches("^\\d{5}$", value);
    }
}
