package prof.code.classes.validation;

import prof.code.interfaces.IDefiningDictionaryWordValidator;

public class CommonDefiningDictionaryWordValidator implements IDefiningDictionaryWordValidator {
    @Override
    public boolean check(String value) {
        return value !=null && !value.contentEquals("");
    }
}
