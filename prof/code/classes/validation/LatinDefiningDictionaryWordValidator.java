package prof.code.classes.validation;

import prof.code.interfaces.IDefiningDictionaryWordValidator;

import java.util.regex.Pattern;

public class LatinDefiningDictionaryWordValidator implements IDefiningDictionaryWordValidator {
    @Override
    public boolean check(String value) {
        return Pattern.matches("^[A-Za-z]{4}$", value);
    }
}
